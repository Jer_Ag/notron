#include "SFML\Graphics.hpp"
#include <windows.h>
#include<iostream>
#include <time.h>


#if _DEBUG
#include "vld.h"
#define DIRECTORY "../Dependencies/Assets/"
#define USE_CONSOLE true
#else
#define DIRECTORY "Assets/"
#define USE_CONSOLE false
#endif




using namespace sf;

const int W = 600;
const int H = 480;
int speed = 4;
bool field[W][H] = { 0 };

struct player
{
	int x, y, dir;

	player()
	{
		y = H / 2;
	}
	void tick()
	{
		if (dir == 0) y += 1;
		if (dir == 1) x -= 1;
		if (dir == 2) x += 1;
		if (dir == 3) y -= 1;

		if (x >= W) x = 0;
		if (x < 0)x = W - 1;
		if (y >= H) y = 0;
		if (y < 0) y = H - 1;
	}
};

int main() 
{
	if (!USE_CONSOLE)
	{
		ShowWindow(GetConsoleWindow(), SW_HIDE);
	}

	srand(time(NULL));
	RenderWindow window(VideoMode(W, H), "NoTron(?");
	window.setFramerateLimit(60);

	Texture background;
	background.loadFromFile(DIRECTORY"background.jpg");
	Sprite sBackground(background);

	Texture player1texture;
	Sprite player1sprite;

	player1texture.loadFromFile(DIRECTORY"bike1.jpg");
	player1sprite.setTexture(player1texture);

	Texture player2texture;
	Sprite player2sprite;

	player2texture.loadFromFile(DIRECTORY"bike2.jpg");
	player2sprite.setTexture(player2texture);

	player p1, p2;
	p1.x = 50;
	p1.dir = 2;
	p2.x = 550;
	p2.dir = 1;

	Sprite sprite;
	RenderTexture texture;
	texture.create(W, H);
	texture.setSmooth(true);
	sprite.setTexture(texture.getTexture());
	texture.clear();
	texture.draw(sBackground);

	Font font;
	font.loadFromFile(DIRECTORY"Sansation.ttf");
	Text text("¡GANASTE!", font, 35);
	Text text2("Presiona *Esc* para salir", font, 20);
	text.setPosition(W / 2 - 100, 20);
	text2.setPosition(W / 2 - 100, 400);

	bool Game = true;

	while (window.isOpen())
	{
		Event e;
		while (window.pollEvent(e))
		{
			if(e.type==Event::Closed)
				window.close();
			if (Keyboard::isKeyPressed(Keyboard::Escape))
				window.close();
		}
		if (Game) {

			if (Keyboard::isKeyPressed(Keyboard::Left)) if (p1.dir != 2) p1.dir = 1;
			if (Keyboard::isKeyPressed(Keyboard::Right)) if (p1.dir != 1)  p1.dir = 2;
			if (Keyboard::isKeyPressed(Keyboard::Up)) if (p1.dir != 0) p1.dir = 3;
			if (Keyboard::isKeyPressed(Keyboard::Down)) if (p1.dir != 3) p1.dir = 0;

			if (Keyboard::isKeyPressed(Keyboard::A)) if (p2.dir != 2) p2.dir = 2;
			if (Keyboard::isKeyPressed(Keyboard::D)) if (p2.dir != 1)  p2.dir = 1;
			if (Keyboard::isKeyPressed(Keyboard::W)) if (p2.dir != 0) p2.dir = 3;
			if (Keyboard::isKeyPressed(Keyboard::S)) if (p2.dir != 3) p2.dir = 0;

			for (int i = 0; i < speed; i++)
			{
				p1.tick(); p2.tick();
				if (field[p1.x][p1.y] == 1) Game = 0;
				if (field[p2.x][p2.y] == 1) Game = 0;
				field[p1.x][p1.y] = 1;
				field[p2.x][p2.y] = 1;


				player1sprite.setPosition(p1.x, p1.y);
				player2sprite.setPosition(p2.x, p2.y);
				texture.draw(player1sprite);
				texture.draw(player2sprite);
				texture.display();
			}

			if (!Game)
			{
				window.draw(text);
				window.draw(text2);
				window.display();
				continue;
			}

			window.draw(text);
			window.draw(sprite);
			window.display();
		}
	}

	return 0;
}
